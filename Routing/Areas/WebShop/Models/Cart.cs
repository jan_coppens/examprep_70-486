﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Routing.Areas.WebShop.Models
{
    public class Cart
    {
        public List<CartItem> Items { get; set; }
        public decimal TotalPrice { get
            {
                return Items.Sum(x => x.Amount * x.Product.Price);
            }
        }
    }
}
﻿using Routing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Routing.Areas.WebShop.Models
{
    public class CartItem
    {
        public Product Product { get; set; }
        public int Amount { get; set; }
    }
}
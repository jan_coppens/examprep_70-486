﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Routing.Areas.WebShop
{
    public class WebShopAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "WebShop";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            //context.MapRoute("WebShop", "WebShop/{action}", new { Controller = "Cart", Action = "Index"});
        }
    }
}

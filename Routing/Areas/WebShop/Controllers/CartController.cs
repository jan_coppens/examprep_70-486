﻿using Routing.Areas.WebShop.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Routing.Areas.WebShop.Controllers
{
    public class CartController : Controller
    {
        public ActionResult Index()
        {
            var products = ProductRepository.GetProducts().ToList();
            var cart = new Cart();

            cart.Items = new List<CartItem>() {
                new CartItem() { Amount = 3, Product = products[0] },
                new CartItem() { Amount = 1, Product = products[2] },
                new CartItem() { Amount = 2, Product = products[3] },
                new CartItem() { Amount = 1, Product = products[5] }
            };

            return View(cart);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Routing
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //Make htm files accessible, works by default?
            //routes.Ignore("{*allhtml}", new { allHtml = @".*\.htm(/.*)?" });

            #region hidden 1
            //routes.MapRoute(
            //    name: "DefaultProduct",
            //    url: "product/{id}",
            //    defaults: new { controller = "Product", action = "Detail" },
            //    constraints: new { id = @"\d+" } //regex pattern
            //);

            #region hidden 2
            //routes.MapRoute(
            //    name: "ProductByName",
            //    url: "product/{Name}",
            //    defaults: new { controller = "Product", action = "DetailByName" }
            //);
            #endregion
            #endregion

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Product", action = "Index", id = UrlParameter.Optional }
            //);
        }
    }
}

﻿using Routing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Routing.Controllers
{
    public class ProductController : Controller
    {
        public ActionResult Index()
        {
            var products = ProductRepository.GetProducts();
            return View(products);
        }

        public ActionResult Detail(int Id)
        {
            var products = ProductRepository.GetProducts();
            var product = products.FirstOrDefault(x => x.Id.Equals(Id));
            return View(product);
        }

        public ActionResult DetailByName(string Name)
        {
            var products = ProductRepository.GetProducts();
            var product = products.FirstOrDefault(x => x.Name.Equals(Name));
            return View("Detail",product);
        }
    }
}
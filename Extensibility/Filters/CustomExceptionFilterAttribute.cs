﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Extensibility.Filters
{
    public class CustomExceptionFilterAttribute : Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            Debug.WriteLine("custom exception filter handled my exception");

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Write("error occured");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Extensibility.Filters
{
    public class CustomResultFilterAttribute : Attribute, IResultFilter
    {
        Stopwatch watch;

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
            watch.Stop();
            var miliseconds = watch.ElapsedMilliseconds;
            filterContext.HttpContext.Response.Headers.Add("RenderingTime", miliseconds.ToString());
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            watch = new Stopwatch();
            watch.Start();
        }
    }
}

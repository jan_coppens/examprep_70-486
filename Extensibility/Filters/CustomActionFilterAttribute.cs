﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Extensibility.Filters
{
    public class CustomActionFilterAttribute : Attribute, IActionFilter
    {
        Stopwatch watch;

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            watch.Stop();
            var miliseconds = watch.ElapsedMilliseconds;
            filterContext.HttpContext.Response.Headers.Add("ActionExecutionTime", miliseconds.ToString());
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            watch = new Stopwatch();
            watch.Start();
        }
    }
}

﻿using Extensibility.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Extensibility
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new CustomExceptionFilterAttribute());
            filters.Add(new CustomResultFilterAttribute());
            filters.Add(new CustomActionFilterAttribute());
        }
    }
}

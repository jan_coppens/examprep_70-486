﻿using Extensibility.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Extensibility.Controllers
{
    public class HomeController : Controller
    {
        [CustomResultFilter]
        [CustomActionFilter]
        public ActionResult Index()
        {
            Thread.Sleep(500);
            return View();
        }

        [CustomExceptionFilter]
        public ActionResult Error()
        {
            throw new HttpException();
        }
    }
}